/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exerciciothread4;

import java.util.Random;


/**
 *
 * @author User
 */
public class ExercicioThread4 {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        Random rdm = new Random();
        int j = 199;
        int[] vetor;
        vetor = new int[200];
        for(int i=0;i<200;i++){
            vetor[i] = j;
            j--;
        }
        int x = rdm.nextInt(200);
        //thread 1
        new BuscaThread(vetor,x).start();
        
        //thread 2
        BuscaRunnable buscaRunnable = new BuscaRunnable(vetor,x);
        new Thread(buscaRunnable).start();
    }
    
}
