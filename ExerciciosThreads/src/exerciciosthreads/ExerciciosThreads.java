/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exerciciosthreads;

/**
 *
 * @author User
 */
public class ExerciciosThreads {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //thread 1
        new Crescente().start();
        
        //thread 2
        Decrescente descrescente = new Decrescente();
        new Thread(descrescente).start();
    }
    
}
