/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exerciciosthreads;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class Decrescente implements Runnable{
    @Override
    public void run(){
        for(int i=100; i>0; i--){
            System.out.println("Decrescente: "+i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(Decrescente.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
