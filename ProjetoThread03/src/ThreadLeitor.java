
import javax.swing.JOptionPane;


/**
 *
 * @author lhries
 */
public class ThreadLeitor extends Thread{
    private Mensagem mensagem;
    public ThreadLeitor(Mensagem mensagem){
        this.mensagem = mensagem;
    }
    
    @Override
    public void run(){
        for(int i=0;i<10; i++){
            String texto = JOptionPane.showInputDialog("Mensagem: ");
            mensagem.setTexto(texto);
            mensagem.setAtualizada(true);
        }
    }    
}
