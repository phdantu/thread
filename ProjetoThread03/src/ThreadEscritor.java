
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author lhries
 */
public class ThreadEscritor extends Thread{
    private Mensagem mensagem;

    public ThreadEscritor(Mensagem mensagem) {
        this.mensagem = mensagem;
    }
    
    @Override
    public void run(){
        String texto="";
        do{
            if(mensagem.isAtualizada()){
                texto = mensagem.getTexto();
                System.out.println("Mensagem recebida: "+texto);
                mensagem.setAtualizada(false);
            }
            

            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(ThreadEscritor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }while(!texto.equalsIgnoreCase("SAIR"));
    }
}
