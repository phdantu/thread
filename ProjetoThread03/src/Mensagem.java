/**
 *
 * @author lhries
 */
public class Mensagem {
    private String texto="";
    private boolean atualizada=false;

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public boolean isAtualizada() {
        return atualizada;
    }

    public void setAtualizada(boolean atualizada) {
        this.atualizada = atualizada;
    }
    
    
}
