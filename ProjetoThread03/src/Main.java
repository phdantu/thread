
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author lhries
 */
public class Main {
    public static void main(String[] args) {
        Mensagem mensagem = new Mensagem();
        Thread leitor = new ThreadLeitor(mensagem);
        leitor.start();
        Thread escritor = new ThreadEscritor(mensagem);
        escritor.start();
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
}
