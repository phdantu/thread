/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exerciciosthread02;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class Pares extends Thread{
    @Override
    public void run() {
        Random rdm = new Random();
        for(int i=0; i<100; i++){
            if (i % 2 != 1){
                System.out.println("Pares: "+i);
            }
            try {
                Thread.sleep(rdm.nextInt(500));
            } catch (InterruptedException ex) {
                Logger.getLogger(Pares.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
